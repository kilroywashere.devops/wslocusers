package devops.kilroywashere.wslocusers.controllers;


import devops.kilroywashere.wslocusers.models.Utilisateur;
import devops.kilroywashere.wslocusers.models.UtilisateurDto;
import devops.kilroywashere.wslocusers.models.UtilisateurException;
import devops.kilroywashere.wslocusers.models.UtilisateurNotFoundException;
import devops.kilroywashere.wslocusers.services.UtilisateurService;
import io.jsonwebtoken.Claims;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.xml.transform.sax.SAXResult;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

@RestController
@RequestMapping("/api/v1")
public class UtilisateursController {

    // DI par Spring Boot avec le constructeur
    private final UtilisateurService service;
    /**
     * Métriques Micrometer
     */
    Counter usersGetAllCount;

    /**
     * Logging
     */
    private final Logger logger = LoggerFactory.getLogger(UtilisateursController.class);

    public UtilisateursController(
            UtilisateurService service,
            MeterRegistry registry
    ) {
        this.service = service;
        Gauge
                .builder("UtilisateursController.UtilisateursCount", fetchUsersCount())
                .tag("version", "v1")
                .description("Nombre d'utilisateurs dans la BDD")
                .register(registry);
    }
    public Supplier<Number> fetchUsersCount() {
        return service::countUtilisateurs;
    }

    /** Retourne tous les utilisateurs
     *
     * @return Une liste d'objet Utilisateur
     */
    @GetMapping("/users")
    @Secured("ROLE_ADMIN")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Utilisateur> getUtilisateurs() {
        logger.info("UtilisateursController.getUtilisateurs");
        return service.findAll();
    }

    /**
     * Retourne tous les utilisateurs d'une ville
     *
     * @param id Id de la ville
     * @return Une liste d'objet Utilisateur
     */
    @Operation(summary = "Obtention des utilisateurs d'après la ville")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Utilisateurs recherchés",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Utilisateur.class)) }),
    })
    @GetMapping("/users/ville/{id}")
    @Secured("ROLE_ADMIN")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Utilisateur> getUtilisateursByVilleId(@PathVariable String id) {
        return service.findAllByVilleId(id);
    }

    /**
     * Retourne tous les utilisateurs d'après une liste de mail
     *
     * @param mails Liste des mails des utilisateurs à rechercher
     * @return Une liste d'objet Utilisateur
     */
    @Operation(summary = "Obtention des utilisateurs d'après une liste de mail")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Utilisateurs recherchés",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Utilisateur.class)) }),
    })
    @GetMapping("/users/mails/{mails}")
    @Secured("ROLE_ADMIN")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Utilisateur> getUtilisateursByIds(@NotNull @PathVariable String mails) {
        List<String> list = Arrays.asList(mails.split(","));
        return service.findAllUtilisateursByIds(list);
    }

    /** Retourne l'utilisateur identifié par id
     *
     * @param id Mail de l'utilisateur
     * @param authentication Authentification fournie par Spring Boot
     * @return Un objet Utilisateur
     */
    @Operation(summary = "Obtention d'un utilisateur")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Utilisateur recherché",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Utilisateur.class))}),
            @ApiResponse(responseCode = "404", description = "Utilisateur non trouvé",
                    content = @Content)
    })
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/users/{id}")
    public Utilisateur getUtilisateur(@PathVariable String id, Authentication authentication) {
        logger.info("UtilisateursController.getUtilisateur");

        if (! isAdminOrOwner(id, authentication)) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED
            );
        }

        // Recherche de l'utilisateur
        Utilisateur utilisateur = service.findById(id);
        if (utilisateur == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    String.format("L'utilisateur ayant l'id (%s) n'existe pas", id)
            );
        }
        return utilisateur;
    }

    /** Ajoute un utilisateur
     * @implNote Les items et les options ne sont pas enregistrés par cette API
     *
     * @param utilisateurDto Objet UtilisateurDto contenant les informations à ajouter
     * @return Le status HTTP 201 : Created avec l'url de la ressource créée si tout est ok
     *        Le status HTTP BAD_REQUEST sinon
     * @see UtilisateurDto
     */
    @Operation(summary = "Création d'un utilisateur")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Utilisateur créé",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Utilisateur.class)) }),
            @ApiResponse(responseCode = "400", description = "Information utilisateur invalide",
                    content = @Content)
    })
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/users")
    public ResponseEntity<Utilisateur> addUser(
            @CookieValue(value = "csrftoken", defaultValue = "") String csrftoken,
            @Valid @RequestBody UtilisateurDto utilisateurDto
    ) {
        logger.info("UtilisateursController.addUser [{}]", utilisateurDto);

        // Vérification du token
        if (! service.isTokenValid(csrftoken)) {
            logger.warn("Invalid token : {}", csrftoken);
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED
            );
        }

        try {
            // Ajout de l'utilisateur
            Utilisateur utilisateurCreated = service.add(utilisateurDto);
            // TODO : renvoyer l'utilisateur
            URI location = ServletUriComponentsBuilder
                    .fromCurrentRequestUri()
                    .path("/{id}")
                    .buildAndExpand(utilisateurCreated.getMail())
                    .toUri();
            return ResponseEntity.created(location).body(utilisateurCreated);

        } catch (Exception exception) {
            logger.error("UtilisateursController.addUser", exception);

            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    String.format("Utilisateur: %s", utilisateurDto.toString())
            );
        }
    }

    /**
     *
     * @param id Mail de l'utilisateur
     * @param utilisateur Utilisateur à modifier
     * @param authentication Authentification fournie par Spring Boot
     * @return Le status HTTP 200 : OK si l'utilisateur est mis à jour
     *         Le status UNAUTHORIZED si l'utilisateur n'est pas admin ou propriétaire
     *         Le status HTTP BAD_REQUEST si les informations utilisateur sont incorrectes
     * @see UtilisateurDto
     */
    @Operation(summary = "Mise à jour d'un utilisateur")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Utilisateur mis à jour",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Utilisateur.class)) }),
            @ApiResponse(responseCode = "400", description = "Information utilisateur invalide",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Utilisateur non trouvé",
                    content = @Content)
    })
    @ResponseStatus(HttpStatus.OK)
    @Secured(value = "ROLE_USER,ROLE_ADMIN")
    @PutMapping(value = "/users/{id}")
    public Utilisateur updateUser(
            @PathVariable String id,
            @Valid @RequestBody Utilisateur utilisateur,
            Authentication authentication
    ) {
        logger.info("UtilisateursController.updateUser [{}]", utilisateur);
        // Vérification des paramètres
        if (! id.equals(utilisateur.getMail())) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "L'id de l'URL ne correspond pas à l'id du body"
            );
        }

        // L'utilisateur doit être le propriétaire ou être admin
        if (! isAdminOrOwner(id, authentication)) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED
            );
        }

        Utilisateur utilisateurUpdated;
        // Mise à jour de l'utilisateur
        try {
            utilisateurUpdated = service.update(utilisateur);
        } catch (UtilisateurNotFoundException exception) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    String.format("L'utilisateur ayant l'id (%s) n'existe pas", id)
            );
        } catch (Exception exception) {
            logger.error("UtilisateursController.updateUser", exception);

            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    String.format("Utilisateur: %s", utilisateur.toString())
            );
        }

        return utilisateurUpdated;
    }

    @Operation(summary = "Supression d'un utilisateur")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Utilisateur supprimé",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Utilisateur.class)) }),
            @ApiResponse(responseCode = "404", description = "Utilisateur non trouvé",
                    content = @Content)
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Secured("ROLE_ADMIN")
    @DeleteMapping("/users/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable String id) {
        logger.info("UtilisateursController.deleteUser [{}]", id);

        try {
            service.delete(id);

            // HTTP Status Code 204 (NO CONTENT)
            return ResponseEntity.noContent().build();
        } catch (UtilisateurNotFoundException exception) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    String.format("L'utilisateur ayant l'id (%s) n'existe pas", id)
            );

        } catch (UtilisateurException exception) {
            logger.error("UtilisateursController.deleteUser", exception);

            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    String.format("L'utilisateur ayant l'id (%s) n'a pas pu être supprimée", id)
            );
        }
    }


    private boolean isAdminOrOwner(String userId, Authentication authentication) {
        try {
            // Autorisé :
            // - L'utilisateur demandé est l'utilisateur connecté
            // - Role_ADMIN

            // Obtention des infos
            final Claims claims = (Claims) authentication.getPrincipal();
            if (userId.equals(claims.getSubject())) return true;

            // Obtentions des rôles
            List<String> roles = (List<String> ) claims.get("roles");
            if (roles.contains("ROLE_ADMIN")) return true;
        } catch (Exception ignored) {}

        return false;
    }
}
