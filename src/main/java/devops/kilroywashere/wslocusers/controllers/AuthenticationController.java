package devops.kilroywashere.wslocusers.controllers;

import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import devops.kilroywashere.wslocusers.security.AuthenticationRequest;
import devops.kilroywashere.wslocusers.security.AuthenticationResponse;
import devops.kilroywashere.wslocusers.models.Utilisateur;
import devops.kilroywashere.wslocusers.services.JwtTokenService;
import devops.kilroywashere.wslocusers.services.UtilisateurService;


@RestController
@RequestMapping("/api/auth")
public class AuthenticationController {
    // Les classes de service
    private final JwtTokenService jwtTokenService;
    private final UtilisateurService service;
    private final Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

    //@Autowired est fait automatiquement
    public AuthenticationController(UtilisateurService service, JwtTokenService tokenService) {
        this.service = service;
        jwtTokenService = tokenService;
    }

    @PostMapping("/login")
    public ResponseEntity<AuthenticationResponse> authenticate(@Valid @RequestBody AuthenticationRequest authenticationRequest) {
        // La réponse pour l'authentification
        AuthenticationResponse response = new AuthenticationResponse();
        // par défaut, cela se passera mal !
        response.status = false;

        // Juste pour le debug
        logger.info(String.format("AuthenticationController.authenticate Login=%s, Role=%s", authenticationRequest.username, authenticationRequest.role));

        // Authentification utilisateur
        try {
            Utilisateur user = service.authenticateUser(authenticationRequest.username, authenticationRequest.password);
            if (user != null) {
                // Si un rôle est demandé pour l'authentification ...
                if (StringUtils.hasLength(authenticationRequest.role)) {
                    // ... alors si le role fourni est présent dans la liste des roles, on accepte l'authentification
                    if (user.getRoles().contains(authenticationRequest.role.toUpperCase()))
                        response.status = true;
                    // sinon, elle est implicitement refusée
                } else {
                    // Si aucun rôle n'est demandé alors on accepte l'authentification
                    response.status = true;
                }
            }

            if (response.status) {
                // Création du token d'après l'utilisateur
                response.token = jwtTokenService.createToken(user);
                // Envoi de la réponse
                return ResponseEntity.ok().body(response);
            }
        } catch (Exception ex) {
            logger.error("AuthenticationController.authenticate: {}", ex.getMessage());
        }
        logger.info("L'authentification a échoué");

        // L'authentification a échoué
        throw new ResponseStatusException(
                HttpStatus.UNAUTHORIZED,
                HttpStatus.UNAUTHORIZED.getReasonPhrase());
    }
}
