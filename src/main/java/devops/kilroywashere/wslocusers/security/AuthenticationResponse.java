package devops.kilroywashere.wslocusers.security;

public class AuthenticationResponse {
    public boolean status;
    public String token;
    public String message;
}
