package devops.kilroywashere.wslocusers.security;

public class AuthenticationRequest {
    public String username;
    public String password;
    public String role;	// Rôle demandé pour l'authentification (paramètre optionnel)

}
