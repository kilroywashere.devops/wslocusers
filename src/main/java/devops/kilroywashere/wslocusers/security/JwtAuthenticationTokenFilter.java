package devops.kilroywashere.wslocusers.security;

import devops.kilroywashere.wslocusers.services.JwtTokenService;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.SignatureException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class JwtAuthenticationTokenFilter extends GenericFilterBean {

    private static final String BEARER = "Bearer";

    @Autowired
    private JwtTokenService jwtTokenService;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
            throws IOException, ServletException {
        boolean finished = false;

        final HttpServletRequest request = (HttpServletRequest) servletRequest;
        final HttpServletResponse response = (HttpServletResponse) servletResponse;

        // Assume we have only one Authorization header value
        final Optional<String> token = Optional.ofNullable(request.getHeader(HttpHeaders.AUTHORIZATION));

        // Le token doit commencer par Bearer
        if (token.isPresent() && token.get().startsWith(BEARER)) {

            // Obtention du token après le mot Bearer
            String bearerToken = token.get().substring(BEARER.length() + 1);
            String message = null;

            try {
                // Obtention des claims (attributs) du token
                final Jws<Claims> claims = jwtTokenService.validateToken(bearerToken);

                // Obtention des rôles
                List<String> rolesList = claims.getBody().get("roles", ArrayList.class);
                // Transformation des rôles en liste d'objets GrantedAuthority
                String[] roles = new String[rolesList.size()];
                rolesList.toArray(roles);
                List<GrantedAuthority> authorityList = AuthorityUtils.createAuthorityList(roles);
                // Création de l'authentification avec l'id de l'utilisateur et ses rôles
                Authentication authentication = new UsernamePasswordAuthenticationToken(claims.getBody(), null, authorityList);

                // Injection de l'authentification dans le contexte de sécurité de Spring Boot
                //  pour pouvoir utiliser les annotations @Secured
                SecurityContextHolder.getContext().setAuthentication(authentication);
            } // Un ensemble d'erreurs possibles
            catch (SignatureException ex) {
                message = "Invalid JWT signature";
            } catch (MalformedJwtException ex) {
                message = "Invalid JWT token";
            } catch (ExpiredJwtException ex) {
                message = "Expired JWT token";
            } catch (UnsupportedJwtException ex) {
                message = "Unsupported JWT token";
            } catch (IllegalArgumentException ex) {
                message = "JWT claims string is empty.";
            } catch (JwtException exception) {
                message = "JwtException raised.";
            }
            // Si le message est non nul alors il y a eu une erreur donc l'accès est non autorisé
            if (message != null) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, message);
                finished = true;
            }

        }
        if (!finished) {// Spring boot doit continuer les filtres présents dans la chaîne
            chain.doFilter(servletRequest, servletResponse);
            // Le contexte de sécurité n'est pas conservé donc on l'efface
            SecurityContextHolder.getContext().setAuthentication(null);
        }

    }
}
