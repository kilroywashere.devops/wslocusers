package devops.kilroywashere.wslocusers.repositories;

import devops.kilroywashere.wslocusers.models.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UtilisateursRepository extends JpaRepository<Utilisateur, String> {
    Utilisateur findByMail(String mail);
    //List<Utilisateur> findUtilisateurByVilleId(String ville_id);
    List<Utilisateur> findUtilisateursByVilleId(String ville_id);

    Iterable<Utilisateur> findUtilisateursByMailIn(List<String> list);
}