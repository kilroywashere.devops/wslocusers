package devops.kilroywashere.wslocusers.services;

import devops.kilroywashere.wslocusers.models.UtilisateurDto;

import devops.kilroywashere.wslocusers.models.Utilisateur;
import devops.kilroywashere.wslocusers.models.UtilisateurException;

import java.util.List;


public interface UtilisateurService {
    Iterable<Utilisateur> findAll();
    Iterable<Utilisateur> findAllByVilleId(String id);
    Iterable<Utilisateur> findAllUtilisateursByIds(List<String> list);
    Utilisateur findById(String id);

    long countUtilisateurs();

    Utilisateur add(UtilisateurDto utilisateurDto) throws UtilisateurException;
    Utilisateur update(Utilisateur utilisateur) throws UtilisateurException;
    void delete(String id) throws UtilisateurException;

    // Security
    Utilisateur authenticateUser(String username, String password);
    boolean isTokenValid(String csrftoken);
}
