package devops.kilroywashere.wslocusers.services;

import devops.kilroywashere.wslocusers.models.*;
import devops.kilroywashere.wslocusers.repositories.UtilisateursRepository;
import devops.kilroywashere.wslocusers.repositories.VilleRepository;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class UtilisateurServiceImpl implements UtilisateurService {

    @Value("${registerpagetoken}")
    private String registerPageToken;

    private final UtilisateursRepository repository;
    private final VilleRepository villeRepository;
    private final VilleLocalisationService villeLocalisationService;
    private final PasswordEncoder passwordEncoder;
    public UtilisateurServiceImpl(
            UtilisateursRepository repository,
            VilleRepository villeRepository,
            VilleLocalisationService villeLocalisationService,
            PasswordEncoder passwordEncoder) {
        this.repository = repository;
        this.villeRepository = villeRepository;
        this.villeLocalisationService = villeLocalisationService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Iterable<Utilisateur> findAll() {
        return repository.findAll();
    }

    @Override
    public Iterable<Utilisateur> findAllByVilleId(@NotNull String id) {
        return repository.findUtilisateursByVilleId(id);
    }

    @Override
    public Iterable<Utilisateur> findAllUtilisateursByIds(List<String> list) {
        return repository.findUtilisateursByMailIn(list);
    }

    @Override
    public Utilisateur findById(@NotNull String id) {
        return repository.findByMail(id);
    }

    @Override
    public long countUtilisateurs() {
        return repository.count();
    }


    @Override
    public Utilisateur add(UtilisateurDto utilisateurDto) throws UtilisateurException {
        Utilisateur utilisateur = utilisateurDto.toUtilisateur();
        // Cryptage du mot de passe
        utilisateur.setPassword(passwordEncoder.encode(utilisateurDto.getPassword()));
        // uniquement Role_User
        utilisateur.setRoles("ROLE_USER");

        // Vérification de la ville
        String nomVille = utilisateurDto.getNomville(), codePostal = utilisateurDto.getCodepostal();
        if (StringUtils.hasLength(nomVille) && StringUtils.hasLength(codePostal)) {
            Ville villeGPSFound = villeLocalisationService.findById(nomVille, codePostal);
            if (villeGPSFound == null) {
                throw new UtilisateurException("La ville n'existe pas");
            }
            if (! villeGPSFound.getNom().equals(nomVille)){
                throw new UtilisateurException("Le nom de la ville est incorrect");
            }

            // Recherche de la ville
            Ville villeData = villeRepository.findById(villeGPSFound.getId()).orElse(null);
            if (villeData == null) {
                // Ajout de la ville
                villeRepository.save(villeGPSFound);
            }
            utilisateur.setVille(villeGPSFound);
        }

        // Ajout de l'utilisateur
        return repository.save(utilisateur);
    }

    @Override
    public Utilisateur update(Utilisateur utilisateur) throws UtilisateurException {
        if (utilisateur == null) {
            return null;
        }
        Utilisateur utilisateurUpdated = findById(utilisateur.getMail());
        if (utilisateurUpdated == null)
            throw new UtilisateurNotFoundException();

        // Modification de l'utilisateur
        utilisateurUpdated.updateFrom(utilisateur);
        return repository.save(utilisateurUpdated);
    }

    @Override
    public void delete(String id) throws UtilisateurException {
        Utilisateur utilisateur = findById(id);
        if (utilisateur == null)
            throw new UtilisateurNotFoundException();

        // Supression de l'utilisateur
        try {
            repository.delete(utilisateur);
        } catch (Exception exception) {
            throw new UtilisateurException(exception.getMessage());
        }
    }

    @Override
    public Utilisateur authenticateUser(String username, String passwordPlain) {
        Utilisateur utilisateur = findById(username);
        if (utilisateur == null) {
            System.out.println("UtilisateurServiceImpl.authenticateUser Not Found");
            return null;
        }
        if (! passwordEncoder.matches(passwordPlain, utilisateur.getPassword())) {
            System.out.println("UtilisateurServiceImpl.authenticateUser - password not matches");
            return null;
        }

        return utilisateur;
    }

    @Override
    public boolean isTokenValid(String csrftoken) {
        return csrftoken != null && csrftoken.equals(registerPageToken);
    }
}
