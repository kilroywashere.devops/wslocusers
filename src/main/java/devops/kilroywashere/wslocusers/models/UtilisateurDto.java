package devops.kilroywashere.wslocusers.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@Getter
@Setter
public class UtilisateurDto {
    private String mail;
    private String password;
    private String nom;
    private String prenom;
    private String nomville;
    private String codepostal;

    public Utilisateur toUtilisateur() {
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setMail(mail);
        utilisateur.setNom(nom);
        utilisateur.setPrenom(prenom);

        return utilisateur;
    }

    @Override
    public String toString() {
        return "UtilisateurDto{" +
                "mail='" + mail + '\'' +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", nomville='" + nomville + '\'' +
                ", codepostal='" + codepostal + '\'' +
                '}';
    }
}
