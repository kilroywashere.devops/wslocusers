package devops.kilroywashere.wslocusers.models;

public class UtilisateurException extends Exception {
    public UtilisateurException() {}

    public UtilisateurException(String message) {super(message);}
}
