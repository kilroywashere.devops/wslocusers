package devops.kilroywashere.wslocusers.models;

public class UtilisateurNotFoundException extends UtilisateurException {
    public UtilisateurNotFoundException() {
        super("Utilisateur non trouvé");
    }
}
