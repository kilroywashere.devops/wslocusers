package devops.kilroywashere.wslocusers.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "utilisateur")
@Getter
@Setter
public class Utilisateur {
    @Id
    @Column(name = "mail", nullable = false, length = 80)
    private String mail;

    @JsonIgnore
    @Column(name = "password", nullable = false, length = 200)
    private String password;
    @Column(name = "nom", length = 100)
    private String nom;
    @Column(name = "prenom", length = 30)
    private String prenom;
    @JsonIgnore
    @Column(name = "roles", length = 100)
    private String roles;

    /*@ManyToOne(
            fetch = FetchType.EAGER,
            cascade = CascadeType.MERGE,
            optional = true
    )
    @JoinColumn(name = "ville_id")*/
    @ManyToOne(
            fetch = FetchType.EAGER,
            optional = false
    )
    private Ville ville;

    /**
     * Mise à jour de l'objet d'après l'utilisateur passé en paramètre
     * @param utilisateur Les nouvelles valeurs de l'utilisateur
     */
    public void updateFrom(@NotNull Utilisateur utilisateur) {
        ville = utilisateur.getVille();
    }

    @Override
    public String toString() {
        return String.format("Utilisateur{mail='%s', nom='%s', prenom='%s',  roles='%s', ville='%s'}'",
                mail, nom, prenom, roles, (ville==null ? "aucune" : ville.toString()));
    }
}