package devops.kilroywashere.wslocusers.actuator;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class BuildInfoContributor implements InfoContributor {
    @Value(value = "${wslocusers.version}")
    private String version;


    @Value(value = "${wslocusers.datasource.url}")
    private String datasourceUrl;

    @Override
    public void contribute(Info.Builder builder) {
        Map<String, String > data = new HashMap<>();
        data.put("build.version", version);
        data.put("db.url", datasourceUrl);
        builder.withDetail("buildInfo", data);
    }
}

