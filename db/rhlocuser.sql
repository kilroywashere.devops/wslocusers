-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : ven. 27 jan. 2023 à 15:30
-- Version du serveur :  8.0.27
-- Version de PHP : 7.4.3-4ubuntu2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `rhlocuser`
--

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `mail` varchar(80) NOT NULL,
  `password` varchar(150) NOT NULL,
  `nom` varchar(100) DEFAULT NULL,
  `prenom` varchar(30) DEFAULT NULL,
  `roles` varchar(100) DEFAULT 'ROLE_USER',
  `ville_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`mail`, `password`, `nom`, `prenom`, `roles`, `ville_id`) VALUES
('test1@test.com', 'aaa', 'aaa', 'aaa', 'ROLE_USER', NULL),
('test@test.com', 'abcd4ABCD', NULL, NULL, 'ROLE_USER', '34172'),
('testPM1@test.com', 'bcf1640e8518625151211f41c2af0066ced666ec', NULL, NULL, 'ROLE_USER', '34172'),
('testPMNew1@test.com', '$2a$10$mWpwl5nUy1pRXofdSJ5s9eE0otFJO0yPQUYtbvakL2o5l54SNTYJ.', 'NTEST', 'ptest', 'ROLE_USER', '34172');

-- --------------------------------------------------------

--
-- Structure de la table `ville`
--

CREATE TABLE `ville` (
  `id` varchar(20) NOT NULL,
  `codepostal` varchar(20) NOT NULL,
  `nom` varchar(60) NOT NULL,
  `longitude` float NOT NULL,
  `latitude` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `ville`
--

INSERT INTO `ville` (`id`, `codepostal`, `nom`, `longitude`, `latitude`) VALUES
('34172', '34000', 'Montpellier', 3.87048, 43.6105);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`mail`),
  ADD KEY `ville_id` (`ville_id`);

--
-- Index pour la table `ville`
--
ALTER TABLE `ville`
  ADD PRIMARY KEY (`id`);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `utilisateur_ibfk_1` FOREIGN KEY (`ville_id`) REFERENCES `ville` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
